import json
from django.views.generic import (
    ListView as LV
)


class ListView(LV):
    paginate_by = 20
    paginate_options = [20, 50, 100]
    classes = {
        "pagination": {
            "div": "d-inline-block",
            "list": "pagination",
            "link": "page-item",
            "text": "page-item disabled",
            "item_content": "page-link",
            "select": "form-control form-control-sm my-1"
        },
        "filters": {
            "input_div": "form-group",
            "help": "form-text text-muted",
            "input": "form-control form-control-sm",
            "button_div": "btn-group d-flex",
            "apply_button": "btn btn-sm btn-outline-primary flex-fill",
            "clear_button": "btn btn-sm btn-outline-secondary flex-fill",
        }
    }
    filterable = []
    orderable = []
    default_ordering = None

    def get_paginate_by(self, queryset):
        pages = self.request.GET.get('paginate_by')
        if not self.paginate_options or pages is None:
            return self.paginate_by

        if int(pages) in self.paginate_options:
            return pages if int(pages) > 0 else None
        return self.paginate_by

    def get_paginate_orphans(self):
        return self.paginate_orphans

    def get_queryset(self):
        q = super().get_queryset()
        try:
            return q.filter(**self.get_filter_dict())
        except:
            return q.none()

    def get_ordering(self):
        query = self.request.GET
        orders = [
            x for x in query.get('order_by', '').split(',')
            if x in self.orderable + ['-{}'.format(y) for y in self.orderable]
        ]
        if len(orders) == 0:
            orders = self.default_ordering
        return orders

    def get_filter_dict(self):
        query = self.request.GET
        return {f: query[f] for f in self.filterable if f in query}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['paginate_by'] = json.dumps(self.paginate_by)
        context['paginate_options'] = json.dumps(self.paginate_options)
        context['classes'] = json.dumps(self.classes)
        return context
