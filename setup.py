from setuptools import setup

setup(
    name='remede',
    version='0.0.1',
    description='Ramda Mithril Django Compiled JS, Templates, and Views',
    url='',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    license='Unlicense',
    packages=['remede'],
    install_requires=[
        'Django >= 2.0'
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    zip_safe=False
)
